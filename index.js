import express from "express";
import graphQLHttp from "express-graphql";
import schema from "./src/schema.js";
import mongoose from "mongoose";
import cors from "cors";
import expressPlayground from "graphql-playground-middleware-express";
import database from "./config/database.js";

const port = 3099;
// mongoose.Promise = Promise;
mongoose.connect(
  database.mongoConnectionString,
  { useNewUrlParser: true },
  err => {
    if (err) {
      console.log(err);
    } else {
      console.log("db connection is okay");
    }
  },
);

const app = express();
app.use(cors());
app.use(
  "/graphql",
  graphQLHttp({
    schema: schema,
  }),
);
app.get("/playground", expressPlayground({ endpoint: "/graphql" }));
app.listen(port, () => {
  console.log("server running at port", port);
});
