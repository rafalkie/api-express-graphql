import dotenv from "dotenv";
dotenv.config();
// eslint-disable-next-line no-undef
const env = process.env;
export default {
  mongoConnectionString: `mongodb://${env.DATABASE_HOST}:${env.DATABASE_PORT}/${env.DATABASE_NAME}`,
};
