import { GraphQLObjectType, GraphQLSchema } from "graphql";
import mutation from "./graphql/mutations/index.js";
import QueryRootType from "./graphql/queries/index.js";

const AppSchema = new GraphQLSchema({
  query: QueryRootType,
  mutation: new GraphQLObjectType({
    name: "Mutation",
    fields: mutation,
  }),
});

export default AppSchema;
