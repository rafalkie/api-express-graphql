import {
  GraphQLList,
  GraphQLInt,
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
} from "graphql";
import Product from "../../models/Product";
import ProductType from "./ProductType";
import Specification from "../../models/Specification";
import SpecificationType from "./SpecificationType";
import deepExtend from "deep-extend";
import getDiffsHistory from "./history/DiffsHistory";
import getVersionProduct from "./history/VersionProduct";
import getVersionSpecification from "./history/VersionSpecification";

function getAllParentTree(text) {
  let all = [];
  let str = Array.from(text);
  str.forEach((item, index) => {
    if (item === "_" && str[index + 1] === "_") {
      all.push(text.slice(0, index));
    }
  });
  all.push(text);
  return all;
}
const QueryRootType = new GraphQLObjectType({
  name: "AppSchema",
  description: "Application Schema Query Root",
  fields: () => ({
    specification: {
      type: new GraphQLList(SpecificationType),
      description: "List of all Specifiaction",
      args: {
        skip: {
          name: "skip",
          type: GraphQLInt,
        },
        first: {
          name: "first",
          type: GraphQLInt,
        },
        filter: {
          name: "filter",
          type: GraphQLList(GraphQLString),
        },
        id: {
          name: "id",
          type: GraphQLString,
        },
        archived: {
          name: "archived",
          type: GraphQLBoolean,
        },
      },
      resolve: async function(
        root,
        { first = null, skip = null, filter = null, id = null, archived = {} },
      ) {
        let filters = filter ? { key: { $in: filter } } : {};
        let filtersId = id ? { key: id } : {};
        return await Specification.find({
          ...filtersId,
          ...filters,
          ...archived,
        })
          .skip(skip)
          .limit(first)
          .exec();
      },
    },
    specificationJson: {
      type: GraphQLString,
      description: "List of all Specifiaction",
      args: {
        filter: {
          name: "filter",
          type: GraphQLString,
        },
      },
      resolve: async function(root, { filter = null }) {
        const specifications = await Specification.find({
          key: { $in: getAllParentTree(filter) },
        });
        let result = {};
        for (let i = 0; i < specifications.length; i++) {
          if (specifications[i].data)
            result = deepExtend(result, JSON.parse(specifications[i].data));
        }
        return JSON.stringify(result);
      },
    },
    product: {
      type: new GraphQLList(ProductType),
      description: "List of all Product",
      args: {
        skip: {
          name: "skip",
          type: GraphQLInt,
        },
        first: {
          name: "first",
          type: GraphQLInt,
        },
        filter: {
          name: "filter",
          type: GraphQLList(GraphQLString),
        },
        id: {
          name: "id",
          type: GraphQLString,
        },
        archived: {
          name: "archived",
          type: GraphQLBoolean,
        },
      },
      resolve: async function(
        root,
        {
          first = null,
          skip = null,
          filter = null,
          id = null,
          archived = false,
        },
      ) {
        let filters = filter ? { key: { $in: filter } } : {};
        let filtersId = id ? { _id: id } : {};
        return await Product.find({ ...filtersId, ...filters, archived })
          .skip(skip)
          .limit(first)
          .exec();
      },
    },
    productCount: {
      type: GraphQLInt,
      description: "Count Product",
      args: {
        archived: {
          name: "archived",
          type: GraphQLBoolean,
        },
      },
      resolve: async function(root, { archived = false }) {
        return await Product.find({ archived }).count();
      },
    },
    getDiffsHistory,
    getVersionProduct,
    getVersionSpecification,
  }),
});

export default QueryRootType;
