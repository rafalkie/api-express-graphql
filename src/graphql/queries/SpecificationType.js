import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLBoolean,
} from "graphql";

const SpecificationType = new GraphQLObjectType({
  name: "SpecificationType",
  description: "This represent an Specification",
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLString) },
    key: { type: new GraphQLNonNull(GraphQLString) },
    data: { type: GraphQLString },
    product: { type: GraphQLString },
    archived: { type: GraphQLBoolean },
    schemaJson: { type: GraphQLString },
  }),
});

export default SpecificationType;
