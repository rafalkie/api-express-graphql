import {
  GraphQLList,
  GraphQLString,
  GraphQLEnumType,
  GraphQLNonNull,
} from "graphql";
import HistoriesType from "./HistoriesType";
import diffHistory from "mongoose-diff-history/diffHistory";

const ModelEnumType = new GraphQLEnumType({
  name: "ModelEnumType",
  values: {
    PRODUCT: {
      value: "Product",
    },
    SPECIFICATION: {
      value: "Specification",
    },
  },
});

const getDiffsHistory = {
  type: new GraphQLList(HistoriesType),
  description: "Get History",
  args: {
    model: {
      name: "model",
      type: new GraphQLNonNull(ModelEnumType),
    },
    id: {
      name: "id",
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: async function(root, { id = null, model = null }) {
    const history = await diffHistory
      .getDiffs(model, id)
      .then(histories => {
        histories
          .reverse()
          .map(item => (item.diff = JSON.stringify(item.diff)));
        return histories;
      })
      .catch(() => null);
    return history;
  },
};

export default getDiffsHistory;
