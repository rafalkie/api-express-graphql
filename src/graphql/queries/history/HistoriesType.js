import {
  GraphQLString,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
} from "graphql";

const HistoriesType = new GraphQLObjectType({
  name: "HistoriesType",
  description: "This represent an Histories Type",
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLString) },
    collectionId: { type: new GraphQLNonNull(GraphQLString) },
    collectionName: { type: GraphQLString },
    diff: {
      type: GraphQLString,
    },
    version: { type: GraphQLInt },
    reason: { type: GraphQLString },
    createdAt: { type: GraphQLString },
    updatedAt: { type: GraphQLString },
  }),
});

export default HistoriesType;
