import { GraphQLInt, GraphQLString, GraphQLNonNull } from "graphql";
import Product from "../../../models/Product";
import ProductType from "../ProductType";
import diffHistory from "mongoose-diff-history/diffHistory";

const getHistoryVersionProduct = {
  type: ProductType,
  description: "Get History",
  args: {
    id: {
      name: "id",
      type: new GraphQLNonNull(GraphQLString),
    },
    version: {
      name: "version",
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async function(root, { id = null, version = 0 }) {
    const history = await diffHistory
      .getVersion(Product, id, version)
      .then(oldObject => {
        return oldObject;
      })
      .catch(() => null);
    return history;
  },
};

export default getHistoryVersionProduct;
