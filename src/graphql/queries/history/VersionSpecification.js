import { GraphQLInt, GraphQLString, GraphQLNonNull } from "graphql";
import SpecificationType from "../SpecificationType";
import Specification from "../../../models/Specification";
import diffHistory from "mongoose-diff-history/diffHistory";

const getHistoryVersionSpecification = {
  type: SpecificationType,
  description: "Get History",
  args: {
    id: {
      name: "id",
      type: GraphQLString,
    },
    version: {
      name: "version",
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async function(root, { id = null, version = 0 }) {
    const history = await diffHistory
      .getVersion(Specification, id, version)
      .then(oldObject => {
        return oldObject;
      })
      .catch(() => null);
    return history;
  },
};

export default getHistoryVersionSpecification;
