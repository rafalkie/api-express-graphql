import {
  GraphQLString,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLBoolean,
} from "graphql";

const ProductType = new GraphQLObjectType({
  name: "ProductType",
  description: "This represent an Product",
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLString) },
    key: { type: new GraphQLNonNull(GraphQLString) },
    data: { type: GraphQLString },
    archived: { type: GraphQLBoolean },
  }),
});

export default ProductType;
