import { GraphQLNonNull, GraphQLString, GraphQLList } from "graphql";
import ProductType from "../queries/ProductType";
import Product from "../../models/Product";
import Specification from "../../models/Specification";

export const addProduct = {
  type: ProductType,
  args: {
    key: {
      name: "key",
      type: new GraphQLNonNull(GraphQLString),
    },
    data: {
      name: "data",
      type: GraphQLString,
    },
  },
  resolve: async function(root, params) {
    let { key } = params;
    params.data = JSON.stringify({ [key]: { id: `${key}` } });
    const uModel = new Product(params);
    const newProduct = await uModel.save();
    if (!newProduct) {
      throw new Error("Error");
    }
    const uModelSpecification = new Specification({
      product: newProduct._id,
      key,
    });
    const newSpecification = await uModelSpecification.save();
    if (!newSpecification) {
      throw new Error("Error");
    }
    return newProduct;
  },
};

export const updateProduct = {
  type: ProductType,
  args: {
    _id: {
      name: "_id",
      type: new GraphQLNonNull(GraphQLString),
    },
    key: {
      name: "key",
      type: GraphQLString,
    },
    data: {
      name: "data",
      type: GraphQLString,
    },
  },
  resolve: async function(root, param) {
    let updateProduct = {};
    let reason = "Aktualizacja struktury";
    if (param.key) {
      updateProduct.key = param.key;
    }
    if (param.data) {
      updateProduct.data = param.data;
    }
    const uProduct = await Product.findOneAndUpdate(
      { _id: param._id },
      updateProduct,
      { new: true, __reason: reason, runValidators: true },
    );
    if (!uProduct) {
      throw new Error("Error");
    }
    return uProduct;
  },
};

export const deleteProduct = {
  type: ProductType,
  args: {
    _id: {
      name: "_id",
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: async function(root, param) {
    const deleteProduct = await Product.findByIdAndUpdate(
      param._id,
      { archived: true },
      { new: true, __reason: `Zarchiwizowanie produktu` },
    );
    await Specification.find({
      product: param._id,
    }).update({ archived: true }, { __reason: `Zarchiwizowanie produktu` });
    if (!deleteProduct) {
      throw new Error("Error");
    }
    return deleteProduct;
  },
};
export const deleteManyProduct = {
  type: GraphQLString,
  args: {
    id: {
      name: "id",
      type: new GraphQLNonNull(GraphQLList(GraphQLString)),
    },
  },
  resolve: async function(root, param) {
    let deleteProduct = await Product.deleteMany({ _id: { $in: param.id } });
    if (!deleteProduct) {
      throw new Error("Error");
    }
    return deleteProduct.deletedCount;
  },
};
