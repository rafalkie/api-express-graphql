import { GraphQLNonNull, GraphQLString, GraphQLList } from "graphql";
import SpecificationType from "../queries/SpecificationType";
import Specification from "../../models/Specification";

export const addSpecification = {
  type: SpecificationType,
  args: {
    key: {
      name: "key",
      type: new GraphQLNonNull(GraphQLString),
    },
    product: {
      name: "product",
      type: GraphQLString,
    },
    data: {
      name: "data",
      type: GraphQLString,
    },
    schemaJson: {
      name: "schemaJson",
      type: GraphQLString,
    },
  },
  resolve: async function(root, params) {
    const uModel = new Specification(params);
    const newSpecification = await uModel.save();
    if (!newSpecification) {
      throw new Error("Error");
    }
    return newSpecification;
  },
};
export const changeKeySpecifications = {
  type: GraphQLString,
  description: "Change keys for all child",
  args: {
    key: {
      name: "key",
      type: new GraphQLNonNull(GraphQLString),
    },
    newKey: {
      name: "newKey",
      type: new GraphQLNonNull(GraphQLString),
    },
    product: {
      name: "product",
      type: new GraphQLNonNull(GraphQLString),
    },
  },

  resolve: async function(root, params) {
    const { newKey, key, product } = params;
    await Specification.find(
      { key: { $regex: `${key}$|${key}__` }, product },
      async (err, result) => {
        await result.forEach(async item => {
          item.key = item.key.replace(key, newKey);
          await Specification.update({ _id: item._id }, { key: item.key });
        });
      },
    ).exec();
  },
};
export const copySpecifications = {
  type: GraphQLString,
  description: "Copy Specification  all child:(param path)",
  args: {
    path: {
      name: "path",
      type: new GraphQLNonNull(GraphQLString),
    },
    newPath: {
      name: "newPath",
      type: new GraphQLNonNull(GraphQLString),
    },
    productCopy: {
      name: "productCopy",
      type: new GraphQLNonNull(GraphQLString),
    },
    productPaste: {
      name: "productPaste",
      type: new GraphQLNonNull(GraphQLString),
    },
  },

  resolve: async function(root, params) {
    const { path, newPath, productCopy, productPaste } = params;
    await Specification.find(
      { key: { $regex: path }, product: productCopy },
      async (err, result) => {
        const newProduct = result.map(item => ({
          key: item.key.replace(`${path}`, `${newPath}Copy`),
          data: item.data ? item.data : null,
          schemaJson: item.schemaJson ? item.schemaJson : null,
          product: productPaste,
        }));
        newProduct.forEach(async item => {
          const uModel = new Specification(item);
          await uModel.save();
        });
      },
    ).exec();
  },
};
export const deleteKeySpecifications = {
  type: GraphQLString,
  description: "Delete keys for all child",
  args: {
    key: {
      name: "key",
      type: new GraphQLNonNull(GraphQLString),
    },
    product: {
      name: "product",
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: async function(root, params) {
    const { key, product } = params;
    await Specification.find({
      key: { $regex: `${key}$|${key}__` },
      product,
    }).remove();
  },
};
export const updateSpecification = {
  type: SpecificationType,
  args: {
    _id: {
      name: "_id",
      type: GraphQLString,
    },
    key: {
      name: "key",
      type: new GraphQLNonNull(GraphQLString),
    },
    data: {
      name: "data",
      type: GraphQLString,
    },
    product: {
      name: "product",
      type: GraphQLString,
    },
    schemaJson: {
      name: "schemaJson",
      type: GraphQLString,
    },
  },
  resolve: async function(root, param) {
    let updateSpecification = {};
    const reason = "Aktualizacja";
    if (param.key) {
      updateSpecification.key = param.key;
    }
    if (param.data) {
      updateSpecification.data = param.data;
    }
    if (param.product) {
      updateSpecification.product = param.product;
    }
    if (param.schemaJson) {
      updateSpecification.schemaJson = param.schemaJson;
    }
    const uSpecification = await Specification.findOneAndUpdate(
      { key: param.key },
      updateSpecification,
      { new: true, __reason: reason },
    );
    if (!uSpecification) {
      throw new Error("Error");
    }
    return uSpecification;
  },
};

export const deleteSpecification = {
  type: SpecificationType,
  args: {
    _id: {
      name: "_id",
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: async function(root, param) {
    const deleteSpecification = await Specification.findByIdAndRemove(
      param._id,
    );
    if (!deleteSpecification) {
      throw new Error("Error");
    }
    return deleteSpecification;
  },
};
export const deleteManySpecification = {
  type: GraphQLString,
  args: {
    id: {
      name: "id",
      type: new GraphQLNonNull(GraphQLList(GraphQLString)),
    },
  },
  resolve: async function(root, param) {
    let deleteSpecification = await Specification.deleteMany({
      _id: { $in: param.id },
    });
    if (!deleteSpecification) {
      throw new Error("Error");
    }
    return deleteSpecification.deletedCount;
  },
};
