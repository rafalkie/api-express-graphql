import { GraphQLNonNull, GraphQLString } from "graphql";
import Specification from "../../models/Specification";
import Product from "../../models/Product";
import objectPath from "object-path";
import { isEmpty } from "../helper";

export const updateTreeStructure = {
  type: GraphQLString,
  description: "Update or add Specification and update Product",
  args: {
    product: {
      name: "product",
      type: new GraphQLNonNull(GraphQLString),
    },
    data: {
      name: "data",
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: async function(root, args) {
    const data = JSON.parse(args.data);
    const { add, deleted, nameChange } = data;

    let product = await Product.findById({ _id: args.product });
    product = JSON.parse(product.data);

    try {
      if (!isEmpty(add)) {
        let keysAdd = Object.keys(add);
        const keysAddReplace = keysAdd.map(item => ({
          key: item.replace(/\./g, "__"),
          product: args.product,
        }));
        const created = await Specification.create(keysAddReplace);
        for (let i = 0; i < created.length; i++) {
          objectPath.set(product, `${keysAdd[i]}`, { id: created[i]._id });
        }
      }

      if (!isEmpty(deleted)) {
        const idDeleted = deleted.flatMap(item => item.id);
        for (let i = 0; i < deleted.length; i++) {
          objectPath.del(product, deleted[i].path);
        }
        Specification.deleteMany({
          _id: { $in: idDeleted },
        });
      }

      if (!isEmpty(nameChange)) {
        //Update all Specification  and Tree
        let products = Object.keys(nameChange);

        //UpdateTree
        for (let i = 0; i < products.length; i++) {
          const item = products[i];
          let { path, newKey } = nameChange[item];
          const copyDataforPath = objectPath.get(product, `${item}`);
          objectPath.del(product, `${item}`);
          let resultNewKey =
            newKey === path ? `${newKey}` : `${path}.${newKey}`;
          objectPath.set(product, resultNewKey, copyDataforPath);
        }
      }
      return Product.findOneAndUpdate(
        { _id: args.product },
        { data: JSON.stringify(product) },
      );
    } catch (err) {
      return err;
    }
  },
};
