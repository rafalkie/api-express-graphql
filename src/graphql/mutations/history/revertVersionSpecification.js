import { GraphQLInt, GraphQLString, GraphQLNonNull } from "graphql";
import SpecificationType from "../../queries/SpecificationType";
import Specification from "../../../models/Specification";
import diffHistory from "mongoose-diff-history/diffHistory";

const revertVersionSpecification = {
  type: SpecificationType,
  description: "Revert History",
  args: {
    id: {
      name: "id",
      type: GraphQLString,
    },
    version: {
      name: "version",
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async function(root, { id = null, version = 0 }) {
    const history = await diffHistory
      .getVersion(Specification, id, version)
      .then(async oldObject => {
        console.log(oldObject);
        return await Specification.findOneAndUpdate(
          { _id: oldObject._id },
          oldObject,
          { __reason: `Cofniecie zmian do wersji ${version}`, new: true },
        ).exec();
      })
      .catch(() => null);
    return history;
  },
};

export default revertVersionSpecification;
