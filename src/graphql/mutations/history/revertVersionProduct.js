import { GraphQLInt, GraphQLString, GraphQLNonNull } from "graphql";
import Product from "../../../models/Product";
import ProductType from "../../queries/ProductType";
import diffHistory from "mongoose-diff-history/diffHistory";

const revertVersionProduct = {
  type: ProductType,
  description: "Revert History",
  args: {
    id: {
      name: "id",
      type: GraphQLString,
    },
    version: {
      name: "version",
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async function(root, { id = null, version = 0 }) {
    const history = await diffHistory
      .getVersion(Product, id, version)
      .then(async oldObject => {
        return await Product.findOneAndUpdate(
          { _id: oldObject._id },
          oldObject,
          { __reason: `Cofniecie zmian do wersji ${version}`, new: true },
        ).exec();
      })
      .catch(() => null);
    return history;
  },
};

export default revertVersionProduct;
