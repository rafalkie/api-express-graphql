import {
  addSpecification,
  updateSpecification,
  deleteSpecification,
  deleteManySpecification,
  changeKeySpecifications,
  deleteKeySpecifications,
  copySpecifications,
} from "./SpecificationMutation";
import { updateTreeStructure } from "./TreeStructureMutation";
import {
  addProduct,
  updateProduct,
  deleteProduct,
  deleteManyProduct,
} from "./ProductMutation";

import revertVersionSpecification from "./history/revertVersionSpecification";

import revertVersionProduct from "./history/revertVersionProduct";

export default {
  changeKeySpecifications,
  copySpecifications,
  addSpecification,
  updateSpecification,
  deleteSpecification,
  deleteManySpecification,
  deleteKeySpecifications,
  deleteManyProduct,
  addProduct,
  updateProduct,
  deleteProduct,
  revertVersionSpecification,
  revertVersionProduct,
  updateTreeStructure,
};
