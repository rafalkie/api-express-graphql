import mongoose from "mongoose";
import { Schema } from "mongoose";
import diffHistory from "mongoose-diff-history/diffHistory";

const productSchema = new Schema({
  key: { type: String, required: true, unique: true },
  data: { type: String, default: "" },
  archived: { type: Boolean, default: false },
});
productSchema.plugin(diffHistory.plugin, { omit: ["key"] });

const Product = mongoose.model("Product", productSchema);

export default Product;
