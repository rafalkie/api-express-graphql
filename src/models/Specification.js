import mongoose from "mongoose";
import { Schema } from "mongoose";
import diffHistory from "mongoose-diff-history/diffHistory";

const specificationSchema = new Schema({
  key: { type: String, required: true },
  product: { type: Schema.Types.ObjectId, ref: "Product" },
  data: { type: String, default: "" },
  schemaJson: { type: String, default: "" },
  archived: { type: Boolean, default: false },
});

specificationSchema.index({
  key: "text",
});

specificationSchema.plugin(diffHistory.plugin, { omit: ["key"] });
const Specification = mongoose.model("Specification", specificationSchema);

export default Specification;
